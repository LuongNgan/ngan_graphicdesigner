var home = {
    init: function() {
        this.header();
        this.footer();
    },
    header: function() {
        $('.iconBar').click(function() {
            $('.mainMenu').slideToggle();
            $(this).toggleClass('changeIconBar');
        })
        $(window).resize(function() {
            if ($(window).width() > 768) {
                $('.mainMenu').css("display", "block");
            }
        })
    },
    footer: function() {

    }
}
home.init();